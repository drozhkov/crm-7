/* global localStorage */

import axios from 'axios';
import { DIT_DATA, SERVER_API_URL } from '../constants';

export function requestAPI(urlParam = '') {
  return function innerFunction(cbs) {
    return axios(`${SERVER_API_URL}${urlParam}`).then(res => {
      if (cbs) cbs.forEach(cb => cb(res.data));
      return res.data;
    });
  };
}

export function updateStateAfterRequest(data) {
  this.data = data;
  this.setState({ dataLoaded: true });
}

export function writeDataToLS(data) {
  localStorage.setItem(DIT_DATA, JSON.stringify(data));
}
