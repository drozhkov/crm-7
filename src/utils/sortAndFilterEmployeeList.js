import { cloneDeep } from 'lodash';
import { ID, NAME } from '../constants';

/**
 * I don't think that using filter three times is actually a good idea.
 */
export default function sortAndFilterEmployeeList(employeeList, options) {
  const output = employeeList.map(employee => cloneDeep(employee));

  return output
    .sort((a, b) => {
      switch (options.sortBy) {
        case NAME:
          return a.secondName.localeCompare(b.secondName, 'en');
        case ID:
        default:
          return a.id - b.id;
      }
    })
    .filter(employee => employee.departments.includes(options.filterByDepartment))
    .filter(employee => {
      if (options.filterByVacation) return employee.vacationDate;
      return employee;
    })
    .filter(employee => {
      if (options.searchTerm.length) {
        return (
          employee.firstName.toLowerCase().indexOf(options.searchTerm.toLowerCase()) !== -1
          || employee.secondName.toLowerCase().indexOf(options.searchTerm.toLowerCase()) !== -1
        );
      }
      return employee;
    });
}
