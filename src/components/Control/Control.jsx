import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import style from './Control.css';
import Button from '../Button/Button';

/**
 * It's good component to think about.
 * Class doesn't seem to solve lot's of bindings.
 */

const Control = ({
  setSort, setFilterByVacation, sortTitle, sortBy, filterByVacation
}) => {
  const setSortBinded = setSort.bind(null, sortBy);
  const setFilterByVacationBinded = setFilterByVacation.bind(null, !filterByVacation);
  const buttonClasses = cx({
    [style.control__button]: true,
    [style.control__button_active]: filterByVacation
  });

  /* eslint-disable */
  return (
    <div className={style.cotrol}>
      <div className={style.cotrol__inner}>
        <span>Sort by:</span>
        <Button type="button" mod={style.control__button} clickHandler={setSortBinded}>
          {sortTitle}
        </Button>
        <span className={style.control__span}>Filter by:</span>
        <Button type="button" mod={buttonClasses} clickHandler={setFilterByVacationBinded}>
          On Vacation
        </Button>
        <span className={style.control__span}>Sorted by: {sortBy}</span>
      </div>
    </div>
  );
  /* eslint-enable */
};

Control.propTypes = {
  setSort: PropTypes.func.isRequired,
  setFilterByVacation: PropTypes.func.isRequired,
  sortTitle: PropTypes.PropTypes.string.isRequired,
  sortBy: PropTypes.PropTypes.string.isRequired,
  filterByVacation: PropTypes.bool.isRequired
};

export default Control;
