import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import style from './Button.css';

const Button = ({
  type, children, mod, clickHandler
}) => {
  const buttonClasses = cx(style.button, mod);

  return (
    // eslint-disable-next-line react/button-has-type
    <button type={type} className={buttonClasses} onClick={clickHandler}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  mod: undefined,
  clickHandler: undefined
};

Button.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  mod: PropTypes.string,
  clickHandler: PropTypes.func
};

export default Button;
